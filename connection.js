'use strict';

const mongoose = require('mongoose');
const config   = require('./config');

const connection = connect();

connection
  .on('error', console.error)
  .on('disconnected', connect);

function connect () {
  let options = {
    server: {
      autoReconnect: true,
      socketOptions: {
        noDelay: true,
        keepAlive: 1,
        socketTimeoutMS: 0
      }
    }
  };
  mongoose.Promise = Promise;
  return mongoose.connect(config.database, options).connection;
}

module.exports = connection;
