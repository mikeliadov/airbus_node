'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const config = require('./config');
const router = require('./router');
const connection = new require('./connection');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors({
  origins: ['http://localhost:3000'],
  credentials: true
}));
app.set('trust proxy', true);

app.use(morgan(':remote-addr - :date[iso] :method :url :status :res[content-length] - :response-time ms'));

router(app);

connection.once('open', initServer);

function initServer () {
  app.listen(config.port);
  console.log(`Hey, magic happens on port ${config.port}`);
}
