'use strcit';

const express = require('express');
const router = express.Router();
const User = require('../../../models/user');

router.post('/', createUser);

function createUser (req, res) {
  console.log('save user api call', req.body);

  User.update({ email: req.body.email }, req.body, {upsert: true})
    .then(result => res.json({message: 'User saved'}))
    .catch(error => res.status(500).json(error));
}

module.exports = router;
