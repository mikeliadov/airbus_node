'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
  name:    {type: String, maxlength: 250, trim: true, required: true},
  email:   {type: String, maxlength: 250, trim: true, required: true, unique: true},
  website: {type: String, maxlength: 250, trim: true},
  phone:   {type: String, maxlength: 24, trim: true},
});

module.exports = mongoose.model('User', UserSchema);
